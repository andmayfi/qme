require 'sinatra'
require 'sinatra/activerecord'
require './config/environments'
require './models/user'
require 'bcrypt'

get '/users' do
  User.all.to_json
end

get '/users/:id' do
  u = User.find(params[:id])
  return status 404 if u.nil?
  u.to_json
end

post '/users' do
  u = User.new(params[:user])
  u.save
  status 201
end

put '/users/:id' do
  u = User.find(params[:id])
  return status 404 if u.nil?
  u.update(params[:user])
  u.save
  status 202
end

delete '/users/:id' do
  u = User.find(params[:id])
  return status 404 if u.nil?
  u.delete
  status 202
end
